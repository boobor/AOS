package cn.osworks.aos.system.dao.po;

import cn.osworks.aos.core.typewrap.PO;

/**
 * <b>test[test]数据持久化对象</b>
 * <p>
 * 注意:此文件由AOS平台自动生成-禁止手工修改。
 * </p>
 * 
 * @author AHei
 * @date 2015-09-08 22:35:13
 */
public class TestPO extends PO {

	private static final long serialVersionUID = 1L;

	/**
	 * id_
	 */
	private Integer id_;
	
	/**
	 * name_
	 */
	private String name_;
	
	/**
	 * sex_
	 */
	private String sex_;
	
	/**
	 * age_
	 */
	private Integer age_;
	

	/**
	 * id_
	 * 
	 * @return id_
	 */
	public Integer getId_() {
		return id_;
	}
	
	/**
	 * name_
	 * 
	 * @return name_
	 */
	public String getName_() {
		return name_;
	}
	
	/**
	 * sex_
	 * 
	 * @return sex_
	 */
	public String getSex_() {
		return sex_;
	}
	
	/**
	 * age_
	 * 
	 * @return age_
	 */
	public Integer getAge_() {
		return age_;
	}
	

	/**
	 * id_
	 * 
	 * @param id_
	 */
	public void setId_(Integer id_) {
		this.id_ = id_;
	}
	
	/**
	 * name_
	 * 
	 * @param name_
	 */
	public void setName_(String name_) {
		this.name_ = name_;
	}
	
	/**
	 * sex_
	 * 
	 * @param sex_
	 */
	public void setSex_(String sex_) {
		this.sex_ = sex_;
	}
	
	/**
	 * age_
	 * 
	 * @param age_
	 */
	public void setAge_(Integer age_) {
		this.age_ = age_;
	}
	

}